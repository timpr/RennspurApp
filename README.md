Rennspur is a system for visualizing the traces of races at sporting events such as sailing races. Built with Java by the MiA17 @LetteVerein.
https://github.com/britzke/rennspur

This is the GPS-Sender App for IOS in Swift.


:zap: :zap: :zap: :zap:

Added Values via App Link
Example:
```
rennspur://?host=URLofServer&hash=TheHASH
```
:zap: :zap: :zap: :zap: