//
//  ViewController.swift
//  Rennspur
//
//  Created by Tim Prangel on 21.02.17.
//  Copyright © 2017 Tim Prangel. All rights reserved.
//

import UIKit
import CoreLocation

class ViewController: UIViewController, CLLocationManagerDelegate,UITextFieldDelegate {
    
    @IBOutlet weak var sendStatus: UILabel!
    @IBOutlet weak var hashlabel: UILabel!
    @IBOutlet weak var hashField: UITextField!
    @IBOutlet weak var button: UIButton!
    let defaultSession = URLSession(configuration: .default)
    let manager = CLLocationManager()
    var start: Bool = false
    var positions = Array<Dictionary<String,String>>()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        hashField.delegate = self
        manager.delegate = self
        
        // Defines the Accuracy of the GPS module
        manager.desiredAccuracy = kCLLocationAccuracyBest
        //manager.distanceFilter = 1000
        
        // Asks for permission to get the position
        manager.requestAlwaysAuthorization();
        
        // Turns on the Background capability to work in the background
        manager.allowsBackgroundLocationUpdates = true
        manager.pausesLocationUpdatesAutomatically  = false
        
        //        map.showsUserLocation = true
        //        map.setUserTrackingMode(.follow, animated: true)
        
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.settingsUpdated), name: NSNotification.Name(rawValue: "settingsUpdated"), object: nil)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    // Hide the keyboard on return press
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.hashField.resignFirstResponder()
        return true
    }

 
    func settingsUpdated()
    {
        self.hashField.text = Settings.instance.hash1
        
        self.hashField.isEnabled = (Settings.instance.hash1.isEmpty)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        let settings = Settings.instance
        
        let location = locations[0]
        
        positions.append(   ["longitude":(String(location.coordinate.longitude)),
                             "latitude":(String(location.coordinate.latitude)),
                             "time":(String(Int64(location.timestamp.timeIntervalSince1970 * 1000.0)))])
        
        
        let params: Dictionary<String, Any> = ["hash":hashField.text!,"positions": positions]
        
        
        let speed = (location.speed) * 3.6
        var request  = URLRequest(url: URL(string: "http://" + settings.host + ":8080/rennspur/rest/gps-service")!)
        NSLog("%@", settings.host)

        
        do{
            
            let data = try JSONSerialization.data(withJSONObject: params , options: [])
            let dataString = String(data: data,encoding: String.Encoding.utf8)!
            
            request.httpMethod = "POST"
            request.httpBody = data
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            
            var dataTask :URLSessionDataTask?
            dataTask = URLSessionDataTask()
            
            dataTask = defaultSession.dataTask(with: request,completionHandler:{
                (data: Data?, response:URLResponse?,error:Error?)
                in
                if error == nil{
                    self.positions.removeAll()
                }
                else{
                    NSLog("----")
                    NSLog("\(String(describing: error))")
                    
                }
                NSLog(dataString)
                print(speed)
            }
            )
            
            dataTask?.resume()
        }
        catch{
            print("JSON seri failed: \(error)")
        }
        
        
        
    }
    @IBAction func buttonPressed(_ sender: Any) {
        let text = hashField.text
        
        if start != true {
            
            if (text?.isEmpty)! {
                showErrorAlert()
                return
            }
            
            manager.startUpdatingLocation()
            start = true
            sendStatus.text = "Sending Data..."
            sendStatus.textColor = #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
            button.setTitle("Stop", for: .normal)
            hashField.isEnabled = false
            
        }
        else{
            manager.stopUpdatingLocation();
            start = false
            sendStatus.text = "Not Sending Data..."
            sendStatus.textColor = #colorLiteral(red: 1, green: 0.6165580153, blue: 0, alpha: 1)
            button.setTitle("Start", for: .normal)
            hashField.isEnabled = true
            
        }
        
    }
    
    // Shows a error when the teamhash field is empty
    func showErrorAlert()
    {
        let alert = UIAlertController(title: "Team-ID Error", message: "The Team-ID field is empty", preferredStyle: .alert)
        
        let alertAction = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
            
        }
        
        alert.addAction(alertAction)
        
        self.present(alert, animated: true) {
            
        }
    }
}

